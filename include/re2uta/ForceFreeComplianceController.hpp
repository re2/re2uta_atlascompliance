/*
 * ForceFreeComplianceController.hpp
 *
 *  Created on: Jun 12, 2013
 *      Author: Isura Ranatunga
 */

#pragma once

#include <rbdl/rbdl.h>
#include "rbdl/rbdl_utils.h"
#include "rbdl_urdfreader.h"

#include <re2uta/AtlasLookup.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <re2uta/AtlasLookup.hpp>
#include <re2/eigen/eigen_util.h>
#include <ros/ros.h>

#include <re2uta/ContactForceDistribution.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>

#include <Eigen/Core>

// TODO lets see why this fails
//#include <rbdl/rbdl.h>


namespace re2uta
{

class ForceFreeComplianceController
{
    public:
        typedef boost::shared_ptr<ForceFreeComplianceController> Ptr;

    public:
        ForceFreeComplianceController();
        void setStateMsg( const atlas_msgs::AtlasStateConstPtr & stateMsg );

        void setCommandMsg( const atlas_msgs::AtlasCommandConstPtr & commandMsg );


        void updateParams( const atlas_msgs::AtlasStateConstPtr & stateMsg,
                           const atlas_msgs::AtlasCommandConstPtr & commandMsg,
                           double dt );

        Eigen::VectorXd calcTorques( double dt );

        void setKp( double Kp );
        void setKv( double Kv );

        void setfilterPosGain( double filterPosGain );
        void setfilterVelGain( double filterVelGain );
        void setfilterForGain( double filterForGain );


    protected:
        ros::NodeHandle                        m_node;
        atlas_msgs::AtlasStateConstPtr        m_lastAtlasState;
        atlas_msgs::AtlasCommandConstPtr      m_lastAtlasCommand;

        re2uta::ContactForceDistribution::Ptr m_contactForceDistribution;


        RigidBodyDynamics::
        Model                  m_atlasModel;
        int                     m_atlasModelDofCount;

        Eigen::VectorXd      m_desiredPositions;
        Eigen::VectorXd      m_positions;
        double               m_filterPosGain;
        Eigen::VectorXd      m_desiredVelocities;
        Eigen::VectorXd      m_velocities;
        double               m_filterVelGain;
        double               m_filterForGain;
        Eigen::Vector3d      m_gravityInImu;
        double                 m_Kp;
        double                 m_Kv;

        RigidBodyDynamics::Math::VectorNd
        URDF_to_RBDL( RigidBodyDynamics::Math::VectorNd& URDF );

        RigidBodyDynamics::Math::VectorNd
        RBDL_to_URDF( RigidBodyDynamics::Math::VectorNd& RBDL );

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}
