/*
 * ContactForceDistribution.hpp
 *
 *  Created on: Jun 12, 2013
 *      Author: Isura Ranatunga
 */

#pragma once

#include <re2uta/AtlasLookup.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <re2uta/AtlasLookup.hpp>
#include <re2/eigen/eigen_util.h>
#include <ros/ros.h>

#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Point.h>

#include <Eigen/Core>

namespace re2uta
{

enum FootSupport {SUPPORT_DOUBLE, SUPPORT_SINGLE_RIGHT, SUPPORT_SINGLE_LEFT};

class ContactForceDistribution
{
    public:
        typedef boost::shared_ptr<ContactForceDistribution> Ptr;

    public:
        ContactForceDistribution();

        visualization_msgs::Marker getCom2ContactLineMarker(  std::string baseName, Eigen::Vector3d & comPosition );
        visualization_msgs::Marker getContactForceLineMarker( std::string baseName, Eigen::VectorXd & desACF_fS   );

        Eigen::MatrixXd calculateZContactForceDistributionMatrix( Eigen::Vector3d & comPosition );
        Eigen::MatrixXd calculateYContactForceDistributionMatrix( Eigen::Vector3d & comPosition );
        Eigen::MatrixXd calculateXContactForceDistributionMatrix( Eigen::Vector3d & comPosition );
        Eigen::MatrixXd calculateFullContactForceDistributionMatrix( Eigen::Vector3d & comPosition );

        Eigen::MatrixXd getA(     Eigen::Vector3d & comPosition );
        Eigen::MatrixXd getPinvA( Eigen::Vector3d & comPosition );

        Eigen::VectorXd reorderContactForceVector2Vec( Eigen::VectorXd & desACF_fS );

        void setlContactPointInBase( Eigen::Affine3d & l_footPos );
        void setrContactPointInBase( Eigen::Affine3d & r_footPos );

        void setSupportMode( FootSupport & support_mode );

        Eigen::Vector3d getlContactPoint1( );
        Eigen::Vector3d getlContactPoint2( );
        Eigen::Vector3d getlContactPoint3( );
        Eigen::Vector3d getlContactPoint4( );

        Eigen::Vector3d getrContactPoint1( );
        Eigen::Vector3d getrContactPoint2( );
        Eigen::Vector3d getrContactPoint3( );
        Eigen::Vector3d getrContactPoint4( );

        Eigen::Vector3d getlContactPoint1InBase( );
        Eigen::Vector3d getlContactPoint2InBase( );
        Eigen::Vector3d getlContactPoint3InBase( );
        Eigen::Vector3d getlContactPoint4InBase( );

        Eigen::Vector3d getrContactPoint1InBase( );
        Eigen::Vector3d getrContactPoint2InBase( );
        Eigen::Vector3d getrContactPoint3InBase( );
        Eigen::Vector3d getrContactPoint4InBase( );


    protected:
        ros::NodeHandle      m_node;
        atlas_msgs::AtlasStateConstPtr   m_lastAtlasState;
        atlas_msgs::AtlasCommandConstPtr m_lastAtlasCommand;

        Eigen::VectorXd      m_desiredPositions;
        Eigen::VectorXd      m_positions;
        Eigen::VectorXd      m_desiredVelocities;
        Eigen::VectorXd      m_velocities;
        Eigen::Vector3d      m_gravityInImu;

        //Position of contact points wrt to foot frame
        Eigen::Vector3d  m_rContactPoint1;
        Eigen::Vector3d  m_rContactPoint2;
        Eigen::Vector3d  m_rContactPoint3;
        Eigen::Vector3d  m_rContactPoint4;

        Eigen::Vector3d  m_lContactPoint1;
        Eigen::Vector3d  m_lContactPoint2;
        Eigen::Vector3d  m_lContactPoint3;
        Eigen::Vector3d  m_lContactPoint4;

        Eigen::Vector3d  m_rContactPoint1InBase;
        Eigen::Vector3d  m_rContactPoint2InBase;
        Eigen::Vector3d  m_rContactPoint3InBase;
        Eigen::Vector3d  m_rContactPoint4InBase;

        Eigen::Vector3d  m_lContactPoint1InBase;
        Eigen::Vector3d  m_lContactPoint2InBase;
        Eigen::Vector3d  m_lContactPoint3InBase;
        Eigen::Vector3d  m_lContactPoint4InBase;

        // Maintains the sate of Atlas support
        FootSupport         m_support_mode;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}
