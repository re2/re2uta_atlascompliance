/**
 *
 * ComStateEstimation.cpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created May 21, 2013
 * @modified May 21, 2013
 *
 */

#include <ros/ros.h>
#include <ros/subscribe_options.h>

#include <re2uta/state_estimate.hpp>

#include <tf/transform_listener.h>

#include <atlas_msgs/AtlasState.h>
#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Vector3.h>

#include <re2uta/AtlasLookup.hpp>
#include <re2uta/calc_point_mass.hpp>
#include <re2uta/center_of_mass_jacobian.hpp>

#include <uta_drc_robotControl/COMData.h>

#include <iostream>

// KDL stuff TODO maybe already included??
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>

using namespace re2uta;

enum PreemptFlag
{
        KeepPublishing    = 0, // 0 - keep publishing (default)
        StopPublishing    = 1, // 1 - stop trajectory pub
        RestartPublishing = 2, // 2 - start/restart trajectory pub
        ResetPublishing   = 3  // 3 - Reset trajectory
};

enum SupportFoot
{
        LeftFoot  = 0,
        RightFoot = 1
};

class AtlasStateEstimationNode
{
        private:

                // Estimator parameters
                Eigen::MatrixXd A ;
                Eigen::MatrixXd B ;
                Eigen::MatrixXd H ;
                Eigen::MatrixXd G ;
                Eigen::MatrixXd Q ;
                Eigen::MatrixXd R ;
                Eigen::MatrixXd X0;
                Eigen::MatrixXd P0;

                Eigen::MatrixXd Z;
                Eigen::MatrixXd U;

                Eigen::MatrixXd X_est;

                // Pointer to Discrete-Time Kalman Filter object
                re2uta::estimate::DiscreteTimeKalmanFilter::Ptr m_kalman;

                double T         ;
                double comHeight ;
                double g         ;
                double wSqr      ;
                double m_comMass ;

                ros::NodeHandle  m_node;
                KDL::Tree        m_tree;
                Eigen::VectorXd  m_treeJointPositions;
                Eigen::VectorXd  m_atlasJointPositions;

                Eigen::VectorXd  m_atlasR2LjointPos;
                Eigen::VectorXd  m_atlasL2RjointPos;

                Eigen::VectorXd  m_jointMins;
                Eigen::VectorXd  m_jointMaxs;

                Eigen::Vector4d  m_comData;

                Eigen::Vector3d  m_comPosition;
                Eigen::Vector3d  m_lastCOMPosition;
                Eigen::Vector3d  m_comVelocity;
                Eigen::Vector3d  m_capturePoint;

                Eigen::Vector3d  m_CoP;

                AtlasLookup::Ptr m_atlasLookup;

                ros::Subscriber       m_atlasStateSubscriber;
                ros::Publisher        m_capturePointPublisher;
                ros::Publisher        m_stopTrajectoryPublisher;
                ros::Publisher        m_comDataPublisher;

                tf::TransformListener m_tfListener;

                ros::Time             m_lastTime;;
                ros::Time             m_sampleTime;

                // Maintains the sate of Atlas support
                SupportFoot           supportFoot;

                // Need to use FK depending on supportFoot to get this
                Eigen::Vector3d l_footPos;
                Eigen::Vector3d r_footPos;

        public:

                /*
                 * This is the constructor for the AtlasCaptureNode
                 */
                AtlasStateEstimationNode()
                {

                        std::string urdfString;

                        m_node.getParam( "/robot_description", urdfString );

                        urdf::Model urdfModel;
                        urdfModel.initString( urdfString );
                        kdlRootInertiaWorkaround( urdfString, m_tree );

                        // State Estimator Variables

                        /* This is the data from the paper
                         * Push Recovery by Stepping for Humanoid Robots with Force Controlled Joints
                         * by Benjamin J. Stephens and Christopher G. Atkenson
                         *
                         */

                        T         = 0.001;
                        comHeight = 0.9;
                        g         = 9.81;
                        wSqr      = g/comHeight;
                        m_comMass = 75;   // total mass

                        A.resize( 8, 8 );
                        A <<      1,    T,       0,             0,      0,     0,       0,             0,
                             wSqr*T,    1, -wSqr*T,   T/m_comMass,      0,     0,       0,             0,
                                  0,    0,       1,             0,      0,     0,       0,             0,
                                  0,    0,       0,             1,      0,     0,       0,             0,
                                  0,    0,       0,             0,      1,     T,       0,             0,
                                  0,    0,       0,             0, wSqr*T,     1, -wSqr*T,   T/m_comMass,
                                  0,    0,       0,             0,      0,     0,       1,             0,
                                  0,    0,       0,             0,      0,     0,       0,             1;

                        B.resize( 8, 2 );
                        B << 0, 0,
                             0, 0,
                             1, 0,
                             0, 0,
                             0, 0,
                             0, 0,
                             0, 1,
                             0, 0;

                        H.resize( 6, 8 );
                        H <<    1,   0,     0,           0,    0,    0,     0,            0,
                                0,   0,     1,           0,    0,    0,     0,            0,
                             wSqr,   0, -wSqr, 1/m_comMass,    0,    0,     0,            0,
                                0,   0,     0,           0,    1,    0,     0,            0,
                                0,   0,     0,           0,    0,    0,     1,            0,
                                0,   0,     0,           0, wSqr,    0, -wSqr,  1/m_comMass;

                        //  G.resize(8,8);
                        G = Eigen::MatrixXd( Eigen::VectorXd::Ones( 8 ).asDiagonal() );

                        Q = Eigen::MatrixXd( Eigen::VectorXd::Ones( 8 ).asDiagonal() );

                        R = Eigen::MatrixXd( Eigen::VectorXd::Ones( 6 ).asDiagonal() );

                        //  X0.resize(8,1);
                        X0 = Eigen::VectorXd::Zero( 8 );

                        X_est = Eigen::VectorXd::Zero( 8 );

                        //  P0.resize(8,8);
                        P0 = Eigen::MatrixXd::Zero( 8, 8 );

                        Z = Eigen::MatrixXd::Zero( 6,1 );

                        U = Eigen::MatrixXd::Zero( 2,1 );

                        // State Estimator Object
                        m_kalman.reset( new re2uta::estimate::DiscreteTimeKalmanFilter( A, B, H, G, Q, R, X0, P0 ) );

                        // State Estimator Variables

                        m_lastCOMPosition = Eigen::Vector3d::Zero();

                        m_atlasLookup.reset( new AtlasLookup( urdfModel ) );
                        m_treeJointPositions   = Eigen::VectorXd::Zero(m_tree.getNrOfJoints());
                        m_atlasJointPositions  = Eigen::VectorXd::Zero(m_tree.getNrOfJoints());
                        m_atlasR2LjointPos     = Eigen::VectorXd::Zero( 12 );
                        m_atlasL2RjointPos     = Eigen::VectorXd::Zero( 12 );
                        m_atlasJointPositions  = Eigen::VectorXd::Zero(m_tree.getNrOfJoints());

                        m_atlasStateSubscriber = m_node.subscribe( "/atlas/atlas_state" , 1,         &AtlasStateEstimationNode::handleAtlasState , this );

                        m_capturePointPublisher = m_node.advertise<visualization_msgs::Marker>("/atlas/capturePoint", 1);
//                        m_stopTrajectoryPublisher = m_node.advertise<std_msgs::Int32>("joint_trajectory_startStop", 1);
                        m_comDataPublisher      = m_node.advertise<uta_drc_robotControl::COMData>("/atlas/comState", 1);

                        supportFoot = RightFoot;

                        // Need to use FK depending on supportFoot to get this
                        l_footPos = Eigen::Vector3d::Zero();
                        r_footPos = Eigen::Vector3d::Zero();

                        m_lastTime = ros::Time::now();

                }


                /*
                 * This will perform the forward kinematics from root to tip of a chain using KDL solvers
                 *
                 */
                Eigen::Vector3d ChainFK( KDL::Tree & in_tree, std::string root, std::string tip, Eigen::VectorXd & atlasJointPos )
                {

                  KDL::Chain leg_chain;
                  in_tree.getChain( root, tip, leg_chain );

                  unsigned int jointNum = leg_chain.getNrOfJoints();
                  KDL::JntArray jointpositions = KDL::JntArray( jointNum );

                  for(unsigned int i = 0; i < jointNum; i++)
                  {
                    jointpositions( i ) = atlasJointPos( i );
                  }

                  // Create solver based on kinematic chain
                  KDL::ChainFkSolverPos_recursive fksolver = KDL::ChainFkSolverPos_recursive( leg_chain );

                  // Create the frame that will contain the results
                  KDL::Frame cartPos;

                  // Calculate forward position kinematics
                  bool kinematics_status;
                  kinematics_status = fksolver.JntToCart( jointpositions, cartPos );

                  return Eigen::Vector3d( cartPos.p(0), cartPos.p(1), cartPos.p(2) );

                }

                /*
                 * This function sets the internal class state that maintains the support state of Atlas
                 * TODO add more sophisticated way to caluclate this, maye include torques etc
                 */
                void setSupportState( double rForceZ )
                {
                  if( rForceZ < 10 )
                    supportFoot = LeftFoot;
                  else
                    supportFoot = RightFoot;
                }

                /*
                 * This function will calculate the CoP aka ZMP for each foot
                 */
                Eigen::Vector3d calcIndividualCoP( geometry_msgs::Wrench wrench )
                {

                  return Eigen::Vector3d( -wrench.torque.y/wrench.force.z, wrench.torque.x/wrench.force.z, -0.079342 );

                }

                /*
                 * This function will calculate the CoP aka ZMP for each foot
                 */
                Eigen::Vector3d calcCombinedCoP( geometry_msgs::Wrench wrenchL, geometry_msgs::Wrench wrenchR, Eigen::Vector3d posL, Eigen::Vector3d posR)
                {

                  Eigen::Vector3d CoPL( calcIndividualCoP( wrenchL ) );
                  Eigen::Vector3d CoPR( calcIndividualCoP( wrenchR ) );

                  double Xc = ( ( posL(0) + CoPL(0) )*wrenchL.force.z + ( posR(0) + CoPR(0) )*wrenchR.force.z ) / (wrenchL.force.z + wrenchR.force.z );
                  double Yc = ( ( posL(1) + CoPL(1) )*wrenchL.force.z + ( posR(1) + CoPR(1) )*wrenchR.force.z ) / (wrenchL.force.z + wrenchR.force.z );

                  return Eigen::Vector3d( Xc, Yc, 0 );

                }

                geometry_msgs::Vector3 EigenVec3toGeomMsgVec3( Eigen::Vector3d inEigenVec3 )
                {

                  geometry_msgs::Vector3 outGeomMsgVec3;
                  outGeomMsgVec3.x = inEigenVec3(0);
                  outGeomMsgVec3.y = inEigenVec3(1);
                  outGeomMsgVec3.z = inEigenVec3(2);

                  return outGeomMsgVec3;

                }


                // Here we set the KDL tree joint states using the Atlas state message
                void handleAtlasState( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg )
                {

                  // SET support state
                  setSupportState( atlasStateMsg->r_foot.force.z );

                  m_sampleTime = ros::Time::now();

                  int msgJointIndex = 0;
                  BOOST_FOREACH( const double & jointPos, atlasStateMsg->position )
                  {
                          int qnr = m_atlasLookup->jointIndexToQnr( msgJointIndex );
                          if( qnr >= 0 && qnr < m_treeJointPositions.rows() )
                                  m_treeJointPositions( qnr ) = atlasStateMsg->position[msgJointIndex];
                                  m_atlasJointPositions( msgJointIndex ) = atlasStateMsg->position[msgJointIndex];

                          ++ msgJointIndex;
                  }

                  std::string     baseName;

                  if( supportFoot == LeftFoot )
                  {
                          baseName            = "l_foot";

                          l_footPos = Eigen::Vector3d::Zero();

                          m_atlasL2RjointPos( 0) = m_atlasJointPositions( 9);  //   4 "l_leg_uhz"
                          m_atlasL2RjointPos( 1) = m_atlasJointPositions( 8);  //   5 "l_leg_mhx"
                          m_atlasL2RjointPos( 2) = m_atlasJointPositions( 7);  //   6 "l_leg_lhy"
                          m_atlasL2RjointPos( 3) = m_atlasJointPositions( 6);  //   7 "l_leg_kny"
                          m_atlasL2RjointPos( 4) = m_atlasJointPositions( 5);  //   8 "l_leg_uay"
                          m_atlasL2RjointPos( 5) = m_atlasJointPositions( 4);  //   9 "l_leg_lax"
                          m_atlasL2RjointPos( 6) = m_atlasJointPositions(10);  //  10 "r_leg_uhz"
                          m_atlasL2RjointPos( 7) = m_atlasJointPositions(11);  //  11 "r_leg_mhx"
                          m_atlasL2RjointPos( 8) = m_atlasJointPositions(12);  //  12 "r_leg_lhy"
                          m_atlasL2RjointPos( 9) = m_atlasJointPositions(13);  //  13 "r_leg_kny"
                          m_atlasL2RjointPos(10) = m_atlasJointPositions(14);  //  14 "r_leg_uay"
                          m_atlasL2RjointPos(11) = m_atlasJointPositions(15);  //  15 "r_leg_lax"

                          r_footPos = ChainFK( m_tree, "l_foot", "r_foot", m_atlasL2RjointPos );

                  }
                  else
                  {
                          baseName            = "r_foot";

                          r_footPos = Eigen::Vector3d::Zero();
//
                          m_atlasR2LjointPos( 0) = m_atlasJointPositions(15);  //   4 "l_leg_uhz"
                          m_atlasR2LjointPos( 1) = m_atlasJointPositions(14);  //   5 "l_leg_mhx"
                          m_atlasR2LjointPos( 2) = m_atlasJointPositions(13);  //   6 "l_leg_lhy"
                          m_atlasR2LjointPos( 3) = m_atlasJointPositions(12);  //   7 "l_leg_kny"
                          m_atlasR2LjointPos( 4) = m_atlasJointPositions(11);  //   8 "l_leg_uay"
                          m_atlasR2LjointPos( 5) = m_atlasJointPositions(10);  //   9 "l_leg_lax"
                          m_atlasR2LjointPos( 6) = m_atlasJointPositions( 4);  //  10 "r_leg_uhz"
                          m_atlasR2LjointPos( 7) = m_atlasJointPositions( 5);  //  11 "r_leg_mhx"
                          m_atlasR2LjointPos( 8) = m_atlasJointPositions( 6);  //  12 "r_leg_lhy"
                          m_atlasR2LjointPos( 9) = m_atlasJointPositions( 7);  //  13 "r_leg_kny"
                          m_atlasR2LjointPos(10) = m_atlasJointPositions( 8);  //  14 "r_leg_uay"
                          m_atlasR2LjointPos(11) = m_atlasJointPositions( 9);  //  15 "r_leg_lax"

                          l_footPos = ChainFK( m_tree, "r_foot", "l_foot", m_atlasR2LjointPos );

                  }


                  m_comData = calcPointMass( m_tree.getSegment( baseName )->second, m_treeJointPositions );

                  // We only care about the first 3 elements x, y, z given by calcPointMass
                  m_comPosition = m_comData.head(3);
                  m_comMass = m_comData(3);

                  // Calculate the combined CoP
                  m_CoP = calcCombinedCoP( atlasStateMsg->l_foot, atlasStateMsg->r_foot, l_footPos, r_footPos );

                  // Update the measurement vector
                  Z << m_comPosition(0),                        // x        - COM X position
                       m_CoP(0),                                // x_c      - CoP X position
                       atlasStateMsg->linear_acceleration.x,    // x_dotdot - COM X acceleration
                       m_comPosition(1),                        // y        - COM Y position
                       m_CoP(1),                                // y_c      - CoP Y position
                       atlasStateMsg->linear_acceleration.y;    // y_dotdot - COM Y acceleration

                  // Update the control input vector
                  // This is the Ux and Uy which are the control inputs to COM
                  U << 0,
                       0;

                  // Kalman Filter Update with Measurement and Control
                  m_kalman->Update( Z, U );
                  X_est = m_kalman->getStateEstimate();

                  // the time step to calculate COM velocity
                  ros::Duration sampleTimeDiff  = m_sampleTime - m_lastTime;
                  m_lastTime                    = m_sampleTime;

                  // This will give the COM velocity
                  // FIXME get COM velocity using COM Jacobian
                  m_comVelocity = (m_comPosition    - m_lastCOMPosition)/sampleTimeDiff.toSec();

                  // This gives the capture point
//                        m_capturePoint = re2uta::calcInstantaneousCapturePoint(m_comPosition, m_comVelocity, comHeight);

//                  // Build and publish capture point marker
//                  visualization_msgs::Marker capturePoint_marker;
//                  capturePoint_marker.header.stamp = atlasStateMsg->header.stamp;
//                  capturePoint_marker.header.frame_id = baseName;
//                  capturePoint_marker.pose.position.x = m_capturePoint(0);
//                  capturePoint_marker.pose.position.y = m_capturePoint(1);
//                  capturePoint_marker.pose.position.z = m_capturePoint(2);
//                  capturePoint_marker.action = visualization_msgs::Marker::ADD;
//                  capturePoint_marker.type = visualization_msgs::Marker::SPHERE;
//                  capturePoint_marker.scale.x = 0.03;
//                  capturePoint_marker.scale.y = 0.03;
//                  capturePoint_marker.scale.z = 0.005;
//                  capturePoint_marker.color.a = 1.0;
//                  capturePoint_marker.color.b = 1.0;
//                  capturePoint_marker.color.r = 1.0;
//                  m_capturePointPublisher.publish(capturePoint_marker);

                  // Setting last COM position
                  m_lastCOMPosition = m_comPosition;

                  // COM State is stored here
                  uta_drc_robotControl::COMData comState_msg;

                  comState_msg.header.stamp = ros::Time::now();

                  // Raw data
                  comState_msg.raw_position = EigenVec3toGeomMsgVec3( m_comPosition );
                  comState_msg.raw_velocity = EigenVec3toGeomMsgVec3( m_comVelocity );
                  comState_msg.raw_cop      = EigenVec3toGeomMsgVec3( m_CoP         );

                  // Estimated data
                  comState_msg.est_position = EigenVec3toGeomMsgVec3( Eigen::Vector3d( X_est( 0 ), X_est( 4 ), 0 ) );
                  comState_msg.est_velocity = EigenVec3toGeomMsgVec3( Eigen::Vector3d( X_est( 1 ), X_est( 5 ), 0 ) );
                  comState_msg.est_cop      = EigenVec3toGeomMsgVec3( Eigen::Vector3d( X_est( 2 ), X_est( 6 ), 0 ) );
                  comState_msg.est_force    = EigenVec3toGeomMsgVec3( Eigen::Vector3d( X_est( 3 ), X_est( 7 ), 0 ) );

                  m_comDataPublisher.publish( comState_msg );

                }

        void go()
        {
                ROS_INFO( "GO!" );
                ros::spin();
        }

};

int main (int argc, char *argv[]) {

  ros::init( argc, argv, "AtlasStateEstimationNode" );

  AtlasStateEstimationNode atlasStateEstimationNode;

  atlasStateEstimationNode.go();

  return 0;

}
