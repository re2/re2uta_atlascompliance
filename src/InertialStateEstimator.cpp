/**
 *
 * InertialStateEstimator.cpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created Jun 4, 2013
 * @modified Jun 4, 2013
 *
 */


#pragma once

#include <re2uta/InertialStateEstimator.hpp>

namespace re2uta
{

namespace estimate
{

  void
  InertialStateEstimator::
  MeasurementUpdate( const atlas_msgs::AtlasState & state )
  {

    double delT = 0.001;

    m_linearAcc(0)  = state.linear_acceleration.x;
    m_linearAcc(1)  = state.linear_acceleration.y;
    m_linearAcc(2)  = state.linear_acceleration.z;

    m_linearVel  = m_linearVel + m_linearAcc*delT;

    // TODO maybe have acceleration here
    m_linearPos  = m_linearPos + m_linearVel*delT;

    // TODO here we are not using angular Acceleration
    m_angularAcc = Eigen::Vector3d::Zero();

//    float yaw, pitch, roll;
//    tf::Quaternion stateOrientation;
//    tf::quaternionMsgToTF( state.orientation, stateOrientation );
//    btMatrix3x3( stateOrientation ).getEulerYPR( yaw, pitch, roll );
//
//    m_angularPos(0) = roll;
//    m_angularPos(1) = pitch;
//    m_angularPos(2) = yaw;

    // FIXME need to fix this
    m_angularPos = Eigen::Vector3d::Zero();

    m_angularVel(0) = state.angular_velocity.x;
    m_angularVel(1) = state.angular_velocity.y;
    m_angularVel(2) = state.angular_velocity.z;

  }

  Eigen::Vector3d
  InertialStateEstimator::
  getLinearPosEstimate()
  {
    return m_linearPos;
  }

  Eigen::Vector3d
  InertialStateEstimator::
  getLinearVelEstimate()
  {
    return m_linearVel;
  }

  Eigen::Vector3d
  InertialStateEstimator::
  getLinearAccEstimate()
  {
    return m_linearAcc;
  }

  Eigen::Vector3d
  InertialStateEstimator::
  getAngularPosEstimate()
  {
    return m_angularPos;
  }

  Eigen::Vector3d
  InertialStateEstimator::
  getAngularVelEstimate()
  {
    return m_angularVel;
  }

  Eigen::Vector3d
  InertialStateEstimator::
  getAngularAccEstimate()
  {
    return m_angularAcc;
  }

}

}
