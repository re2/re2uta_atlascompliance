/*
 * ModelBasedComplianceController.cpp
 *
 *  Created on: Jun 12, 2013
 *      Author: Isura Ranatunga
 */

#include <re2uta/ModelBasedComplianceController.hpp>

#include <atlas_msgs/SetJointDamping.h>

#include <re2uta/AtlasLookup.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <re2uta/AtlasLookup.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/eigen/eigen_util.h>

#include <ros/ros.h>
#include <string>



namespace
{
    template <typename Type>
    Type exponentialWeightedAverage( Type oldVal, Type newVal, double newWeight )
    {
        return oldVal*(1-newWeight) + newWeight*(newVal);
    }

    template <typename Type>
    Type lowPassFilter( Type oldVal, Type newVal, double newWeight )
    {
        return exponentialWeightedAverage( oldVal, newVal, newWeight );
    }

    Eigen::VectorXd subsetInclusive( int baseIndex, int tipIndex, const Eigen::VectorXd & inputVector )
    {
        Eigen::VectorXd outputVector( std::abs( tipIndex - baseIndex  + 1 ) );

        int count = 0;
        for( int i = baseIndex; i <= tipIndex; ++i )
        {
            outputVector( count ) = inputVector( i );
            ++count;
        }

        return outputVector;
    }


    Eigen::VectorXd setSubsetInclusive( int baseIndex, int tipIndex, const Eigen::VectorXd & inputVector, Eigen::VectorXd & outputVector )
    {
        int count = 0;
        for( int i = baseIndex; i <= tipIndex; ++i )
        {
            outputVector( i ) = inputVector( count );
            ++count;
        }

        return outputVector;
    }
}


namespace re2uta
{

// TODO change these to Eigen and/or make smaller
RigidBodyDynamics::Math::VectorNd
ModelBasedComplianceController::
URDF_to_RBDL( RigidBodyDynamics::Math::VectorNd& URDF )
{

  RigidBodyDynamics::Math::VectorNd RBDL( m_atlasModelDofCount, 1 );

  RBDL(  0 )    = URDF(  0 );         RBDL( 14 )    = URDF( 26 );
  RBDL(  1 )    = URDF(  1 );         RBDL( 15 )    = URDF( 27 );
  RBDL(  2 )    = URDF(  2 );         RBDL( 16 )    = URDF( 4  );
  RBDL(  3 )    = URDF( 16 );         RBDL( 17 )    = URDF( 5  );
  RBDL(  4 )    = URDF( 17 );         RBDL( 18 )    = URDF( 6  );
  RBDL(  5 )    = URDF( 18 );         RBDL( 19 )    = URDF( 7  );
  RBDL(  6 )    = URDF( 19 );         RBDL( 20 )    = URDF( 8  );
  RBDL(  7 )    = URDF( 20 );         RBDL( 21 )    = URDF( 9  );
  RBDL(  8 )    = URDF( 21 );         RBDL( 22 )    = URDF( 10 );
  RBDL(  9 )    = URDF(  3 );         RBDL( 23 )    = URDF( 11 );
  RBDL( 10 )    = URDF( 22 );         RBDL( 24 )    = URDF( 12 );
  RBDL( 11 )    = URDF( 23 );         RBDL( 25 )    = URDF( 13 );
  RBDL( 12 )    = URDF( 24 );         RBDL( 26 )    = URDF( 14 );
  RBDL( 13 )    = URDF( 25 );         RBDL( 27 )    = URDF( 15 );

  return RBDL;
}

RigidBodyDynamics::Math::VectorNd
ModelBasedComplianceController::
RBDL_to_URDF( RigidBodyDynamics::Math::VectorNd& RBDL )
{

  RigidBodyDynamics::Math::VectorNd URDF( 28, 1 );

  URDF(  0 ) = RBDL(  0 );            URDF( 26 ) = RBDL( 14 );
  URDF(  1 ) = RBDL(  1 );            URDF( 27 ) = RBDL( 15 );
  URDF(  2 ) = RBDL(  2 );            URDF(  4 ) = RBDL( 16 );
  URDF( 16 ) = RBDL(  3 );            URDF(  5 ) = RBDL( 17 );
  URDF( 17 ) = RBDL(  4 );            URDF(  6 ) = RBDL( 18 );
  URDF( 18 ) = RBDL(  5 );            URDF(  7 ) = RBDL( 19 );
  URDF( 19 ) = RBDL(  6 );            URDF(  8 ) = RBDL( 20 );
  URDF( 20 ) = RBDL(  7 );            URDF(  9 ) = RBDL( 21 );
  URDF( 21 ) = RBDL(  8 );            URDF( 10 ) = RBDL( 22 );
  URDF(  3 ) = RBDL(  9 );            URDF( 11 ) = RBDL( 23 );
  URDF( 22 ) = RBDL( 10 );            URDF( 12 ) = RBDL( 24 );
  URDF( 23 ) = RBDL( 11 );            URDF( 13 ) = RBDL( 25 );
  URDF( 24 ) = RBDL( 12 );            URDF( 14 ) = RBDL( 26 );
  URDF( 25 ) = RBDL( 13 );            URDF( 15 ) = RBDL( 27 );

  return URDF;
}

ModelBasedComplianceController::
ModelBasedComplianceController()
{
    m_filterPosGain = 10;
    m_filterVelGain = 10;
    m_filterForGain = 1000;

    // Position and velocity controller gains
    m_Kp = 180;
    m_Kv = 10;

    // RBDL Start - Init
    std::string urdfString;

//    std::string urdfString;
//    m_node.getParam( "/robot_description", urdfString );
//    m_urdfModel.initString( urdfString );

    m_node.getParam( "/simple_robot_description", urdfString );
//    <!-- Simple Robot Description for RBDL Model -->
//    <param name="simple_robot_description" textfile="$(find uta_drc_robotControl)/atlas.urdf"/>

    m_atlasModel.Init();

    // sets loaded model information printing
    bool verbose = false;

    // Loading of URDF model
    ROS_ASSERT( RigidBodyDynamics::Addons::
                read_urdf_model( urdfString, &m_atlasModel, verbose, "pelvis" ) );

    // Set floating base joint - adds 6 massless bodies with 3 revolute and 3 linear joints
    //  m_atlasModel.SetFloatingBaseBody( m_atlasModel.mBodies[0] );

    m_atlasModelDofCount  = m_atlasModel.dof_count;

}


void
ModelBasedComplianceController::
setKp( double Kp )
{
    m_Kp = Kp;
}

void
ModelBasedComplianceController::
setKv( double Kv )
{
    m_Kv = Kv;
}

void
ModelBasedComplianceController::
setfilterPosGain( double filterPosGain )
{
    m_filterPosGain = filterPosGain;
}

void
ModelBasedComplianceController::
setfilterVelGain( double filterVelGain )
{
    m_filterVelGain = filterVelGain;
}

void
ModelBasedComplianceController::
setfilterForGain( double filterForGain )
{
    m_filterForGain = filterForGain;
}


void
ModelBasedComplianceController::
setStateMsg( const atlas_msgs::AtlasStateConstPtr & stateMsg )
{
    boost::atomic_store( &m_lastAtlasState, stateMsg );
}


void
ModelBasedComplianceController::
setCommandMsg( const atlas_msgs::AtlasCommandConstPtr & commandMsg )
{
    boost::atomic_store( &m_lastAtlasCommand, commandMsg );
}


void
ModelBasedComplianceController::
updateParams( const atlas_msgs::AtlasStateConstPtr & stateMsg,
              const atlas_msgs::AtlasCommandConstPtr & commandMsg,
              double dt )
{
    Eigen::VectorXd newPositions  = re2uta::parseJointPositions(  stateMsg );
    Eigen::VectorXd newVelocities = re2uta::parseJointVelocities( stateMsg );

    if( m_positions.size() == 0 )
        m_positions = newPositions;
    if( m_velocities.size() == 0 )
        m_velocities = newVelocities;

    m_positions  = newPositions ;            // m_positions  = lowPassFilter( m_positions,  newPositions,  m_filterPosGain*dt );
    m_velocities = newVelocities;            // m_velocities = lowPassFilter( m_velocities, newVelocities, m_filterVelGain*dt );

    if( commandMsg )
    {
        m_desiredPositions  = re2uta::parseJointPositions(  commandMsg );
        m_desiredVelocities = re2uta::parseJointVelocities( commandMsg );
    }
    else
    if( m_desiredPositions.size() == 0 )
    {
        m_desiredPositions  = m_positions;
        m_desiredVelocities = m_velocities;
    }
}

Eigen::VectorXd
ModelBasedComplianceController::
calcTorques( double dt )
{
    atlas_msgs::AtlasStateConstPtr   atlasState;
    atlas_msgs::AtlasCommandConstPtr atlasCommand;
    atlasState   = boost::atomic_load( &m_lastAtlasState   );
    atlasCommand = boost::atomic_load( &m_lastAtlasCommand );


    if( !atlasState ) // || !atlasCommand )
        return Eigen::VectorXd();

    updateParams( atlasState, atlasCommand, dt );

    typedef atlas_msgs::AtlasState Atlas;


    // RBDL Model Based Controller

    RigidBodyDynamics::Math::VectorNd Q         = RigidBodyDynamics::Math::VectorNd::Zero ( m_atlasModelDofCount );
    RigidBodyDynamics::Math::VectorNd QDot      = RigidBodyDynamics::Math::VectorNd::Zero ( m_atlasModelDofCount );
    RigidBodyDynamics::Math::VectorNd QDDot     = RigidBodyDynamics::Math::VectorNd::Zero ( m_atlasModelDofCount );

    RigidBodyDynamics::Math::VectorNd QDes      = RigidBodyDynamics::Math::VectorNd::Zero ( m_atlasModelDofCount );
    RigidBodyDynamics::Math::VectorNd QDotDes   = RigidBodyDynamics::Math::VectorNd::Zero ( m_atlasModelDofCount );
    RigidBodyDynamics::Math::VectorNd QDDotDes  = RigidBodyDynamics::Math::VectorNd::Zero ( m_atlasModelDofCount );

    RigidBodyDynamics::Math::VectorNd Tau       = RigidBodyDynamics::Math::VectorNd::Zero ( m_atlasModelDofCount );
    RigidBodyDynamics::Math::VectorNd cTau      = RigidBodyDynamics::Math::VectorNd::Zero ( m_atlasModelDofCount ); // Contact torque


    // Set the gravity vector in the IMU frame
    Eigen::Quaterniond gravityOrientInInertial( atlasState->orientation.w,
                                                atlasState->orientation.x,
                                                atlasState->orientation.y,
                                                atlasState->orientation.z );

    Eigen::Vector3d gravityInImu = gravityOrientInInertial.inverse()*Eigen::Vector3d ( 0, 0, -9.81 );
    m_atlasModel.gravity = gravityInImu;

    Eigen::VectorXd q        = m_positions;
    Eigen::VectorXd qDes     = Eigen::VectorXd::Zero( 28 );
    Eigen::VectorXd q_dot    = m_velocities;
    Eigen::VectorXd qDes_dot = Eigen::VectorXd::Zero( 28 );

    if( true /*atlasState*/ )
    {
        for( unsigned int i = 0; i < atlasState->position.size(); ++i )
        {
          qDes(i)     = atlasCommand->position[i];
          qDes_dot(i) = atlasCommand->velocity[i];
        }
    }


    Q       = URDF_to_RBDL( q          );
    QDes    = URDF_to_RBDL( qDes     );
    QDot    = URDF_to_RBDL( q_dot    );
    QDotDes = URDF_to_RBDL( qDes_dot );


    // FT CUttoff: 1000 | Kp: 180.000000 | Kv: 10.000000
    QDDot = QDDotDes + m_Kv*(QDotDes - QDot) + m_Kp*(QDes - Q);


    RigidBodyDynamics::InverseDynamics ( m_atlasModel, Q, QDot, QDDot,  Tau); //, &f_ext);



    unsigned int         body_id;
    const RigidBodyDynamics::Math::Vector3d point_position(0, 0, 0);

    RigidBodyDynamics::Math::Vector3d nowlFootForce(  atlasState->l_foot.force.x,  atlasState->l_foot.force.y,  atlasState->l_foot.force.z  );
    RigidBodyDynamics::Math::Vector3d nowrFootForce(  atlasState->r_foot.force.x,  atlasState->r_foot.force.y,  atlasState->r_foot.force.z  );
    RigidBodyDynamics::Math::Vector3d nowlFootMoment( atlasState->l_foot.torque.x, atlasState->l_foot.torque.y, atlasState->l_foot.torque.z );
    RigidBodyDynamics::Math::Vector3d nowrFootMoment( atlasState->r_foot.torque.x, atlasState->r_foot.torque.y, atlasState->r_foot.torque.z );


    RigidBodyDynamics::Math::Vector3d zmpL( -nowlFootMoment(1)/nowlFootForce(2), nowlFootMoment(0)/nowlFootForce(2), -0.079342f );
    RigidBodyDynamics::Math::Vector3d zmpR( -nowrFootMoment(1)/nowrFootForce(2), nowrFootMoment(0)/nowrFootForce(2), -0.079342f );

    RigidBodyDynamics::Math::MatrixNd J_right( 3, m_atlasModelDofCount );
    RigidBodyDynamics::Math::MatrixNd J_left(  3, m_atlasModelDofCount );

    RigidBodyDynamics::CalcPointJacobian ( m_atlasModel, Q, 28,  zmpR, J_right, true );
    RigidBodyDynamics::CalcPointJacobian ( m_atlasModel, Q, 22,  zmpL, J_left,  true );


    // Contact condition
    if( nowlFootForce(2) < 10 )
    {
//      nowrFootForce(1) = -2*zmpR(1);
      nowlFootForce(2) = 0;
    }


    if( nowrFootForce(2) < 10 )
    {
//      nowlFootForce(1) = -2*zmpL(1);
      nowrFootForce(2) = 0;
    }


    cTau = J_left.transpose()*nowlFootForce + J_right.transpose()*nowrFootForce;
    Tau  = Tau - cTau;


    int numJoints = 28; // FIXME this shouldn't be hard coded here;
    Eigen::VectorXd torques = Eigen::VectorXd::Zero( numJoints );

    torques = RBDL_to_URDF( Tau );

    return torques;
}


}
