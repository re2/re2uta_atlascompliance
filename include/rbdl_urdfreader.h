#ifndef _RBDL_URDFREADER_H
#define _RBDL_URDFREADER_H

namespace RigidBodyDynamics {

namespace Addons {

    bool read_urdf_float_model (const char* filename, Model* model, bool verbose = false, std::string baseLink = "pelvis");

    bool read_urdf_model (std::string & urdfString, Model* model, bool verbose = false, std::string baseLink = "pelvis");
}

}

/* _RBDL_URDFREADER_H */
#endif
